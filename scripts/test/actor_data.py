from ely.direct.animTransition import AnimTransition
from direct.actor.Actor import Actor

actorData = {
    'models': 'eve',
    'anims': {
        'jump': 'eve-jump',
        'run': 'eve-run',
        'walk': 'eve-walk',
        'offbalance': 'eve-offbalance',
    },
}


class AnimToFrame(object):

    def __init__(self, from_cycle_length, to_cycle_length, delta):
        self.to_cycle_length = to_cycle_length
        self.from_cycle_length = from_cycle_length
        self.delta = delta

    def __call__(self, animFromFrame):
        frameFrom = animFromFrame % self.from_cycle_length
        frameTo = (frameFrom * self.to_cycle_length / self.from_cycle_length +
                   self.delta)
        if frameTo >= self.to_cycle_length:
            frameTo -= self.to_cycle_length
        return frameTo


def getPlayRateRatios(fromCycleLen, toCycleLen):
    return (fromCycleLen / toCycleLen, toCycleLen / fromCycleLen)


run_cycle_length = 16
walk_cycle_length = 24.0
walk_run_delta = 1.0
run_walk_delta = 0.0

animToFrameWR = AnimToFrame(
    walk_cycle_length, run_cycle_length, walk_run_delta)
playRateRatiosWR = getPlayRateRatios(walk_cycle_length, run_cycle_length)
animToFrameRW = AnimToFrame(
    run_cycle_length, walk_cycle_length, run_walk_delta)
playRateRatiosRW = getPlayRateRatios(run_cycle_length, walk_cycle_length)

animTransitionsData = {
    'walk-run': {
        'type': AnimTransition,
        'animCtrlFrom': lambda actor: Actor.get_anim_control(actor, 'walk'),
        'animCtrlTo': lambda actor: Actor.get_anim_control(actor, 'run'),
        'animToFrame': animToFrameWR,
        'blendType': None,
        'frameBlend': None,
        'partName': None,
        'playRate': 1.0,
        'playRateRatios': playRateRatiosWR,
        'usePose': True,
        'loop': True,
        'duration': 1.0,
        # 'doneEvent':'walk_run_done',
    },
    'run-walk': {
        'type': AnimTransition,
        'animCtrlFrom': lambda actor: Actor.get_anim_control(actor, 'run'),
        'animCtrlTo': lambda actor: Actor.get_anim_control(actor, 'walk'),
        'animToFrame': animToFrameRW,
        'blendType': None,
        'frameBlend': None,
        'partName': None,
        'playRate': 1.0,
        'playRateRatios': playRateRatiosRW,
        'usePose': True,
        'loop': True,
        'duration': 1.0,
        # 'doneEvent':'run_walk_done',
    },
}

# other informations
walkSpeed = 6.8
runSpeed = 11.89
angularSpeedFactor = 5.0
