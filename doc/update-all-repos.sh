#/bin/bash

REPO=/REPOSITORY/KProjects
RED='\033[0;31m'
MAG='\033[0;45m'
NC='\033[0m' # No Color

printf "${RED}makehuman${NC}\n"
cd ${REPO}/MAKEHUMAN/makehuman/ && git pull && cd ${REPO}

printf "${RED}makehuman-community-plugins-assetdownload (forked)${NC}\n"
#cd ${REPO}/makehumancommunity/community-plugins-assetdownload/ && git pull && cd ${REPO}
cd ${REPO}/MAKEHUMAN/community-plugins-assetdownload/ && git fetch upstream && git merge upstream/master && cd ${REPO}

printf "${RED}makehuman-community-plugins-socket${NC}\n"
cd ${REPO}/MAKEHUMAN/community-plugins-socket/ && git pull && cd ${REPO}

printf "${RED}makehuman-community-plugins-mhapi${NC}\n"
cd ${REPO}/MAKEHUMAN/community-plugins-mhapi/ && git pull && cd ${REPO}

printf "${RED}makehuman-community-plugins-perspective_view${NC}\n"
cd ${REPO}/MAKEHUMAN/community-plugins-perspective_view/ && git pull && cd ${REPO}

printf "${RED}makehuman-plugin-for-blender${NC}\n"
cd ${REPO}/MAKEHUMAN/makehuman-plugin-for-blender/ && git pull && cd ${REPO}

printf "${RED}community-plugins-assetedit${NC}\n"
cd ${REPO}/MAKEHUMAN/community-plugins-assetedit/ && git pull && cd ${REPO}

printf "${RED}mhx2-makehuman-exchange${NC}\n"
cd ${REPO}/MAKEHUMAN/mhx2-makehuman-exchange/ && hg pull && hg update && cd ${REPO}

printf "${RED}makewalk${NC}\n"
cd ${REPO}/MAKEHUMAN/makewalk/ && hg pull && hg update && cd ${REPO}

printf "${RED}Panda3D-Bam-Exporter${NC}\n"
#cd ${REPO}/P3DBLENDER_PLUGINS/Panda3D-Bam-Exporter/ && git pull && git submodule update --recursive && cd ${REPO}
cd ${REPO}/P3DBLENDER_PLUGINS/Panda3D-Bam-Exporter/ && git fetch upstream && git merge upstream/master && git submodule update --recursive && cd ${REP}

printf "${RED}YABEE${NC}\n"
cd ${REPO}/P3DBLENDER_PLUGINS/YABEE/ && git pull && cd ${REPO}

