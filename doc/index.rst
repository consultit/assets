.. Assets documentation master file, created by
   sphinx-quickstart on Sat Apr 25 11:01:57 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

*****************************
Assets documentation contents
*****************************

Here is the documentation related to **assets** creation, management, distribution.

.. toctree::
   :numbered:
   :caption: Contents:
   
   asset-tutorial

Indices and search
==================

* :ref:`genindex`
* :ref:`search`
