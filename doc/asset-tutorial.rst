***********************
Asset creation tutorial
***********************

.. toctree::
   :maxdepth: 2
   :caption: Content:
   
Introduction
============

To convert this manual in ``texinfo`` format follow these steps:

#.	Export in ``DocBook`` format: ``File -> Export -> DocBook (XML)``

#.	Run these commands:

	::
	
	$ pandoc -f docbook -t texinfo asset-tutorial.xml | makeinfo -o asset-tutorial.info

#.	To see the ``info`` file run:

	::
	
	$ info ./asset-tutorial.info
 
Makehuman
=========

.. index::
   pair: makehuman; installation

Installation
------------

The following steps are performed under an common installation path referred in
the ``MHPATH`` environment variable.

*	Clone ``makehuman`` from source repository into ``$MHPATH``:

	::
	
	$ cd $MHPATH
	$ git clone https://github.com/makehumancommunity/makehuman.git

*	Clone ``makehumancommunity plugins`` from source repositories into
 	``$MHPATH`` [#]_ [#]_:

	::

	$ cd $MHPATH
	$ git clone https://github.com/makehumancommunity/makehuman-plugin-for-blender.git
	$ git clone https://github.com/makehumancommunity/community-plugins-assetedit.git
	$ git clone https://github.com/makehumancommunity/community-plugins-mhapi.git
	$ git clone https://github.com/makehumancommunity/community-plugins-socket.git
	$ git clone https://github.com/makehumancommunity/community-plugins-assetdownload.git
	$ git clone https://github.com/makehumancommunity/community-plugins-perspective_view.git
	$ git clone https://github.com/makehumancommunity/community-plugins-makeclothes.git
	$ git clone https://github.com/makehumancommunity/community-plugins-maketarget.git

*	Clone ``mhx2-makehuman-exchange``, ``makewalk`` and ``retarget-bvh`` plugins
	for ``blender`` from source repositories into ``$MHPATH``:

	::
	
	$ cd $MHPATH
	$ git clone https://bitbucket.org/Diffeomorphic/mhx2-makehuman-exchange.git
	$ git clone https://bitbucket.org/Diffeomorphic/makewalk.git
	$ git clone https://bitbucket.org/Diffeomorphic/retarget-bvh.git

*	Install all the plugins:

	::
	
	$ cd $MHPATH/makehuman/makehuman/plugins
	$ ln -s $MHPATH/community-plugins-mhapi/1_mhapi .
	$ ln -s $MHPATH/community-plugins-assetdownload/8_asset_downloader .
	$ ln -s $MHPATH/community-plugins-assetedit/8_asset_editor .
	$ ln -s $MHPATH/community-plugins-perspective_view/8_perspective_animation.py .
	$ ln -s $MHPATH/community-plugins-socket/8_server_socket .
	$ ln -s $MHPATH/mhx2-makehuman-exchange/9_export_mhx2 .

To execute ``makehuman``:

	::
	
	$ cd $MHPATH/makehuman
	$ makehuman/makehuman &

The first time is created the directory ``$HOME/makehuman`` which contains all
``makehuman``'s related stuff.

Blender
=======

.. index::
   pair: blender 2.7x; installation

Installation (2.7x)
-------------------

This installation refers to ``blender 2.79``.

The following configuration steps are performed under the configuration path
``$HOME/.config/blender/2.79/scripts/addons`` which is referred by the
``BPATH`` environment variable.

*	Get blender either downloading from the `Blender <https://www.blender.org>`_
 	or from the package of a recent ``debian/ubuntu`` distribution and install it

*	Create the path ``$BPATH``

	::
	
	$ mkdir -p $BPATH

*	Install the ``makehuman``'s ``blendertools`` plugins:

	::
	
	$ cd $BPATH
	$ ln -s $MHPATH/makehuman/blendertools/makeclothes .
	$ ln -s $MHPATH/makehuman/blendertools/maketarget .

*	Install the ``makehumancommunity``'s plugin:

	::
	
	$ cd $MHPATH/makehuman-plugin-for-blender/blender_distribution
	$ unzip MH_Community_for_blender_279.zip
	$ mv MH_Community MH_Community279
	$ cd $BPATH
	$ ln -s $MHPATH/makehuman-plugin-for-blender/blender_distribution/MH_Community279 .

*	Install the ``mhx2-makehuman-exchange`` and ``makewalk`` plugin:

	::
	
	$ cd $BPATH
	$ ln -s $MHPATH/mhx2-makehuman-exchange/import_runtime_mhx2 .
	$ ln -s $MHPATH/makewalk .

*	Clone and install the ``panda3d``'s export plugins (these plugins are cloned
	into the common path referred by the ``PPATH`` environment variable) [#]_

	::
	
	$ cd $PPATH
	$ git clone https://github.com/09th/YABEE.git
	$ git clone https://github.com/tobspr/Panda3D-Bam-Exporter.git --recurse-submodules
	$ cd $BPATH
	$ ln -s $PPATH/YABEE .
	$ ln -s $PPATH/Panda3D-Bam-Exporter .

*	Run ``blender``, open ``File->User Preferences ...`` (or ``Ctrl-Alt-U``) and in the
	``Add-ons`` tab enable all the various installed plugins (they are located in
	``MakeHuman`` and ``Import-Export`` categories).

.. index::
   pair: blender 2.8x; installation

Installation (2.8x)
-------------------

This installation refers to ``blender 2.8x``.

The following configuration steps are performed under the configuration path 
``$HOME/.config/blender/2.8x/scripts/addons`` which is referred by the ``BPATH28``
environment variable.

*	Get ``blender`` from the `Blender <https://www.blender.org>`_ site and install it [#]_

*	Create the path ``$BPATH28``

	::
	
	$ mkdir -p $BPATH28

*	Install the ``makehumancommunity``'s plugin:

	::
	
	$ cd $MHPATH/makehuman-plugin-for-blender/blender_distribution
	$ unzip MH_Community_for_blender_280.zip
	$ mv MH_Community MH_Community280
	$ chmod -R a+X MH_Community280
	$ cd $BPATH28
	$ ln -s $MHPATH/makehuman-plugin-for-blender/blender_distribution/MH_Community280 .

*	Install the ``makehuman``'s blendertools plugins:

	::
	
	$ cd $BPATH28
	$ ln -s $MHPATH/community-plugins-makeclothes/makeclothes .
	$ ln -s $MHPATH/community-plugins-maketarget/maketarget .

*	Install the ``mhx2-makehuman-exchange``, ``makewalk`` and ``retarget-bvh`` plugins:

	:: 
	
	$ cd $BPATH28
	$ ln -s $MHPATH/mhx2-makehuman-exchange/import_runtime_mhx2 .
	$ ln -s $MHPATH/makewalk .
	$ ln -s $MHPATH/retarget-bvh .

*	Clone and install the ``panda3d``'s export plugins (these plugins are cloned
	into the common path referred by the ``PPATH`` environment variable)

	::
	
	$ cd $PPATH
	$ git https://github.com/kergalym/YABEE.git YABEE_28
	$ cd $BPATH28
	$ ln -s $PPATH/YABEE_28 .

* 	Run ``blender``, open ``Edit->Preferences...`` and in the ``Add-ons`` tab
	enable all the various installed plugins (they are prefixed with ``MakeHuman:``
	and ``Import-Export:``).

Human animations
================

Some hints for creating human models.

Creating humans
---------------

In ``Makehuman`` download the ``Motion_Builder_Rig`` rig (after downloading you may
need copy/link the files inside ``$HOME/makehuman/v1/data/rigs`` if they don't exist).

Create your model and apply the rig and then export to ``mhx2 (MakeHuman Exchange)``
format.

Applying animations
-------------------

In ``Blender`` import the model in ``mhx2`` format.

To apply animation follow the instructions as explained at:

*	`MakeWalk docs <http://www.makehumancommunity.org/wiki/Documentation:MakeWalk>`_ for ``Blender 2.7x``
*	`BVH Retargeter docs <https://diffeomorphic.blogspot.com/p/bvh-retargeter.html>`_ for ``Blender 2.8x`` [#]_

.. rubric:: Notes

..	[#] Currently the ``community-plugins-assetedit.git`` doesn't work; you could
	clone this alternative `repository <https://gitlab.com/consultit/community-plugins-assetedit.git>`_
	which should correct the issues.	
..	[#] Currently the ``community-plugins-assetdownload`` may have problems displaying
	the description table of available assets; you could clone this alternative 
	`repository <https://gitlab.com/consultit/community-plugins-assetdownload.git>`_
	which should correct the issues.	
..	[#] Currently the ``Panda3D-Bam-Exporter`` can be cloned from 
	`here <https://gitlab.com/consultit/Panda3D-Bam-Exporter.git>`_.
..	[#] Currently neither ``debian`` and ``ubuntu`` have ``2.8x`` version.
..	[#] `MakeWalk docs <http://www.makehumancommunity.org/wiki/Documentation:MakeWalk>`_
	site may still have useful hints.
